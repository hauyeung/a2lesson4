package com.example.a2lesson4;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

public class timecomponent extends TextView{

	boolean do24h = false;

	public timecomponent(Context context, AttributeSet attrs) {
		super(context, attrs);		        
		// TODO Auto-generated constructor stub			

		do24h = attrs.getAttributeBooleanValue("http://schemas.android.com/apk/lib/com.example.a2lesson4.timecomponent", "do24hrs", false);

		Calendar cal = Calendar.getInstance();
		Date t = cal.getTime();																	 				 				
		SimpleDateFormat sdf24 = new SimpleDateFormat("HH:mm:ss");
		String ts24 = sdf24.format(t).toString();
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
		String ts = sdf.format(t).toString();
		if (do24h)
			setText(ts24);
		else
			setText(ts);

	}



}
